#!/bin/bash

dkc="docker-compose -p visitus-fe"

docker --version
$dkc --version

cd ci
$dkc up -d --build
