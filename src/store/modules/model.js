import ApiService from '@/common/api/api.service'

const state = {
  type: null,
  data: null
}

const getters = {
  data: () => state.data
}

const actions = {
  getData ({ commit }, payload) {
    return ApiService.model(state.type).get(payload)
      .then((r) => {
        console.log(r)
        commit('setData', { data: r.data.list })
      })
  }
}

const mutations = {
  setType (state, { type }) {
    state.type = type
    state.data = null
  },

  setData (state, { data }) {
    state.data = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
