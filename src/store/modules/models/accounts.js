import ApiService from '@/common/api/api.service'
const AccountsService = ApiService.model('user')

const state = {
  data: null
}

const actions = {
  get ({ commit }, payload) {
    return AccountsService.get(payload)
      .then((r) => {
        commit('set', { data: r.data.list })
      })
  },

  create ({ commit }, payload) {
    return AccountsService.create(payload)
  }
}

const mutations = {
  set (state, { data }) {
    state.data = data
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
