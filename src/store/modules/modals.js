import Modals from '@/common/modals.service'

const state = {
  activeModals: [],
  alertMessage: ''
}

const getters = {
  alertMessage: () => state.alertMessage
}

const actions = {
  toast ({ commit }, { html }) {
    Modals.toast(html)
  },

  showModal ({ commit }, { id, message }) {
    Modals.open(id)
    // commit('showModal', { id })
    commit('setAlertMessage', { message })
  },

  hideModal ({ commit }, { id }) {
    Modals.close(id)
    commit('hideModal', { id })
  }
}

const mutations = {
  showModal (state, { id }) {
    state.activeModals = state.activeModals.concat(id)
  },

  hideModal (state, { id }) {
    state.activeModals = state.activeModals.filter(modal => modal !== id)
  },

  setAlertMessage (state, { message }) {
    state.alertMessage = message
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
