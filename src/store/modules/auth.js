import ApiService from '@/common/api/api.service'
// import router from '@/router'

const state = {
  isAuthenticated: null
}

const getters = {
  isAuthenticated: () => state.isAuthenticated
}

const actions = {
  isAuthenticated ({ commit }) {
    return ApiService.auth.check().then((r) => {
      commit('isAuthenticated', {
        isAuthenticated: r.status === 200
      })
      return r.status === 200
    })
  },

  login ({ commit, dispatch }, payload) {
    return ApiService.auth.login(payload).then((r) => {
      commit('isAuthenticated', {
        isAuthenticated: r.status === 200
      })
      return r.status === 200
    }).catch((error) => {
      dispatch('toast', {
        html: _.get(error, 'response.data.description')
      })
    })
  },

  logout ({ commit, dispatch }) {
    return ApiService.auth.logout().then((r) => {
      commit('isAuthenticated', {
        isAuthenticated: false
      })
      return false
    }).catch((error) => {
      dispatch('toast', {
        html: _.get(error, 'response.data.description')
      })
    })
  }
}

const mutations = {
  isAuthenticated (state, { isAuthenticated }) {
    state.isAuthenticated = isAuthenticated
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
