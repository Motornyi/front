import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import modals from './modules/modals'
import accounts from './modules/models/accounts'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    modals,
    accounts
  }
})
