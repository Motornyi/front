import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import routes from './routes'
Vue.use(Router)

const router = new Router({
  // mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  const redirect = (isAuthenticated) => {
    if (to.meta.protected !== false && !isAuthenticated) {
      return next('/login')
    }
    if (['/login'].includes(to.fullPath) && isAuthenticated) {
      return next('/')
    }
    next()
  }
  const { isAuthenticated } = store.state.auth
  return isAuthenticated === null
    ? store.dispatch('isAuthenticated').then(redirect)
    : redirect(isAuthenticated)
})

export default router
