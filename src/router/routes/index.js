import login from '@/containers/auth/login'
import home from '@/containers/main/home'
import notFound from '@/containers/404'

import homeChildren from './home'

export default [
  {
    path: '/login',
    name: 'login',
    component: login,
    meta: {
      protected: false
    }
  },
  {
    path: '/',
    redirect: '/widgets',
    name: 'home',
    component: home,
    children: homeChildren
  },
  {
    path: '**',
    name: '404',
    component: notFound
  }
]
