import accounts from '@/containers/main/accounts/accounts'
import account from '@/containers/main/accounts/account'

import widgets from '@/containers/main/widgets/widgets'
import widget from '@/containers/main/widgets/widget'

import mailing from '@/containers/main/mailing'
import payment from '@/containers/main/payment'
import stat from '@/containers/main/stat'
import trip from '@/containers/main/trip'

export default [
  {
    path: '/accounts',
    component: accounts
  },
  {
    path: '/accounts/add',
    component: account
  },
  {
    path: '/accounts/:id',
    component: account
  },

  {
    path: '/widgets',
    component: widgets
  },
  {
    path: '/widgets/add',
    component: widget,
    redirect: '/widgets/add/1'
  },
  {
    path: '/widgets/add/:form',
    component: widget
  },
  {
    path: '/widgets/:id/',
    component: widget,
    redirect: '/widgets/:id/0'
  },
  {
    path: '/widgets/:id/:from',
    component: widget
  },

  {
    path: '/mailing',
    component: mailing
  },
  {
    path: '/payment',
    component: payment
  },
  {
    path: '/stat',
    component: stat
  },
  {
    path: '/trip',
    component: trip
  }
]
