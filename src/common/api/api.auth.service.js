import axios from 'axios'
import { apiUrl } from '../config'

export default {
  check () {
    return axios.get(`${apiUrl}/api/session`)
      .catch(() => false)
  },

  login (data) {
    return axios.post(`${apiUrl}/api/session`, data)
  },

  logout (data) {
    return axios.delete(`${apiUrl}/api/session`)
  }
}
