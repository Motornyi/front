import axios from 'axios'
import { apiUrl } from '../config'

export default (type) => ({
  get (params) {
    return axios.get(`${apiUrl}/api/${type}`)
      .catch(() => false)
  },

  create (data, params) {
    return axios.post(`${apiUrl}/api/${type}`, data)
      .catch(() => false)
  }
})
