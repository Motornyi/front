import auth from './api.auth.service'
import model from './api.model.service'

const ApiService = {
  auth,
  model
}

export default ApiService
