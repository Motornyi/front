const Modals = {
  toast (html) {
    window.M.toast({ html })
  },

  get (id) {
    const el = document.getElementById(id)
    if (!el) { return false }
    return window.M.Modal.getInstance(el) || window.M.Modal.init(el)
  },

  open (id) {
    this.get(id).open()
  },

  close (id) {
    this.get(id).close()
  }
}

export default Modals
